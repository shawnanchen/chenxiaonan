import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
 * @author shawnan
 * @date 2020/7/19 5:31 下午
 * @description
 */
public class JedisClusterMainApp {
    public static void main(String[] args) {
        Set<HostAndPort> nodes = new HashSet<HostAndPort>();
        nodes.add(new HostAndPort("192.168.31.199", 6379));
        nodes.add(new HostAndPort("192.168.31.199", 6380));
        nodes.add(new HostAndPort("192.168.31.165", 6379));
        nodes.add(new HostAndPort("192.168.31.165", 6380));
        nodes.add(new HostAndPort("192.168.31.142", 6379));
        nodes.add(new HostAndPort("192.168.31.142", 6380));
        nodes.add(new HostAndPort("192.168.31.126", 6379));
        nodes.add(new HostAndPort("192.168.31.126", 6380));
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisCluster jedisCluster = new JedisCluster(nodes, jedisPoolConfig);
        jedisCluster.set("name:1", "zhangfei");
        jedisCluster.set("name:2", "zhaoyun");
        jedisCluster.set("name:3", "guanyu");

        System.out.println(jedisCluster.get("name:1"));
        System.out.println(jedisCluster.get("name:2"));
        System.out.println(jedisCluster.get("name:3"));
    }
}
