package com.lagou.pojo;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author shawnan
 * @date 2020/3/14 6:03 下午
 * @description
 */
public class RpcDecoder extends ByteToMessageDecoder {
    private Class<?> clazz;
    private Serializer serializer;

    public RpcDecoder(Class<?> clazz, Serializer serializer) {
        this.clazz = clazz;
        this.serializer = serializer;
    }

    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        if (clazz != null && byteBuf != null && byteBuf.readableBytes() >= 4) {
            int dataLength = byteBuf.readInt(); // 发送的时候先写入了长度，所以读取的时候也要先读取长度
            // 发送的时候写入了一个int，所以从4开始读取长度为dataLength的内容  获取所有接收到的内容
            byte[] bytes = new byte[dataLength];
            byteBuf.readBytes(bytes);
            Object object = serializer.deserialize(clazz, bytes);
            list.add(object);
        }
    }
}
