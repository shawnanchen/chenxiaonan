package com.lagou.pojo;

import java.io.Serializable;
import java.time.Instant;

/**
 * @author shawnan
 * @date 2020/4/4 3:45 下午
 * @description
 */
public class ZNode implements Serializable {
    private static final long serialVersionUID = 1L;
    private String url;
    private Integer port;
    private Instant lastResponseTime;
    private long lastResponseDuration;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Instant getLastResponseTime() {
        return lastResponseTime;
    }

    public void setLastResponseTime(Instant lastResponseTime) {
        this.lastResponseTime = lastResponseTime;
    }

    public long getLastResponseDuration() {
        return lastResponseDuration;
    }

    public void setLastResponseDuration(long lastResponseDuration) {
        this.lastResponseDuration = lastResponseDuration;
    }

    @Override
    public String toString() {
        return "ZNode{" +
                "url='" + url + '\'' +
                ", port=" + port +
                ", lastResponseTime=" + lastResponseTime +
                ", lastResponseDuration=" + lastResponseDuration +
                '}';
    }
}
