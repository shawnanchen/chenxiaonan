package com.lagou.pojo;

import com.alibaba.fastjson.JSON;

/**
 * @author shawnan
 * @date 2020/3/14 5:57 下午
 * @description
 */
public class JSONSerializer implements Serializer{
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}