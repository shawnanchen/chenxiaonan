package com.lagou.pojo;

import java.io.Serializable;

/**
 * @author shawnan
 * @date 2020/3/14 5:51 下午
 * @description
 */
public class RpcResponse implements Serializable {
    private String requestId;
    private Exception exception;
    private Object result;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
