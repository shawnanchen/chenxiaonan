package com.lagou.service;

import com.lagou.handler.UserServerHandler;
import com.lagou.pojo.*;
import com.lagou.util.ServerUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service("userService")
public class UserServiceImpl implements UserService {
    static {
        try {
            String url = ServerUtil.url;
            int port = ServerUtil.port;
            startServer(url, port);
            System.out.println("Server started on + " + url + ":" + port);
            ServerUtil.registerServer();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String sayHello(String word) {
        Random random = new Random();
        long sleepTime = Math.abs(random.nextLong() % 10) * 1000;
        // 随机休眠10s内的一个时间，模拟响应时长
        System.out.println("休眠" + sleepTime);
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "调用成功--参数 " + word;
    }

    //hostName:ip地址  port:端口号
    public static void startServer(String hostName,int port) throws InterruptedException {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup,workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new RpcEncoder(RpcResponse.class, new JSONSerializer()));
                        pipeline.addLast(new UserServerHandler());
                    }
                });
        serverBootstrap.bind(hostName, port).sync();
    }
}
