package com.lagou.handler;

import com.lagou.pojo.RpcRequest;
import com.lagou.pojo.RpcResponse;
import com.lagou.util.ApplicationContextUtil;
import com.lagou.util.ServerUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;

public class UserServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object object) throws Exception {
        Instant startInsant = Instant.now();
        // 判断是否符合约定，符合则调用本地方法，返回数据
        RpcRequest request = (RpcRequest)object;
        String className = request.getClassName();
        String methodName = request.getMethodName();
        Class<?>[] parameterTypes = request.getParameterTypes();
        Object[] parameters = request.getParameters();
        ClassLoader classLoader = ApplicationContextUtil.getApplicationContext().getClassLoader();
        Class<?> clazz = classLoader.loadClass(className);
        Object obj = ApplicationContextUtil.getBean(clazz);
        RpcResponse response = new RpcResponse();
        response.setRequestId(request.getRequestId());
        if (obj == null) {
            Exception exception = new Exception("class not found");
            response.setException(exception);
            ctx.writeAndFlush(response);
            return;
        }

        // 此处必须加参数才能获取到方法
        Method method = clazz.getMethod(methodName, parameterTypes);
        method.setAccessible(true); // 防止私有方法
        Object result = method.invoke(obj, parameters);
        System.out.println(LocalDateTime.now().toLocalTime() + ":" + request.toString());
        response.setResult(result);
        ctx.writeAndFlush(response);
        Instant endInstant = Instant.now();
        long seconds = Duration.between(startInsant, endInstant).getSeconds();
        ServerUtil.updateLastResponseDuration(seconds);
    }
}
